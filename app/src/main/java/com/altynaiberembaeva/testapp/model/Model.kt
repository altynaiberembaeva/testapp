package com.altynaiberembaeva.testapp.model

import com.altynaiberembaeva.testapp.R
import com.altynaiberembaeva.testapp.data.ProductCategory
import com.altynaiberembaeva.testapp.data.Products

class Model {

    val productCategoryList = ArrayList<ProductCategory>()
    val productsList = ArrayList<Products>()

    constructor() {
        productCategory()
        products()
    }

        fun productCategory(){
            productCategoryList.add(ProductCategory(1, " Fruits "))
            productCategoryList.add(ProductCategory(2, " Vegetables "))
            productCategoryList.add(ProductCategory(3, " Dairy products "))
            productCategoryList.add(ProductCategory(4, " Bakery "))
            productCategoryList.add(ProductCategory(5, " Flowers "))
        }

        fun products(){

            //Fruits
            productsList.add(Products(1, "Свежий арбуз", "Австралийские арбузы, сочные", "26 KGZ", R.drawable.watermelon_800))
            productsList.add(Products(1, "Талаские яблоки", "Сочные яблоки по доступным ценам", "20 KGZ", R.drawable.apple))
            productsList.add(Products(1, "Нарынская груша", "Вкусные груши, сочные", "15 KGZ", R.drawable.grusha))
            productsList.add(Products(1, "Сочные персики", "Сочные, вкусные персики", "30 KGZ", R.drawable.persiki))
            productsList.add(Products(1, "Мандарины", "Вкусные мандарины, по доступной цене", "36 KGZ", R.drawable.orange))
            productsList.add(Products(1, "Бананы ", "Вкусные, спелые бананы", "200 KGZ", R.drawable.banana))
            productsList.add(Products(1, "Черешня сладкая", "Домашняя вкусная черешня", "26 KGZ", R.drawable.chery))
            productsList.add(Products(1, "Чуйская груша", "Домашняя Чуйская груша", "36KGZ", R.drawable.grusha2))
            productsList.add(Products(1, "Слива сладкая", "Спелая, сладкая слива", "46 KGZ", R.drawable.sliva))
            productsList.add(Products(1, "Виноград ", "Сладкий виноград", "26 KGZ", R.drawable.vinograd))

            //Vegetables
            productsList.add(Products(2, "Баклажан", "Бишкекские баклажаны", "20 KGZ", R.drawable.baklajan))
            productsList.add(Products(2, "Картошка", "Картошка по низкой цене", "15 KGZ", R.drawable.kartoshka))
            productsList.add(Products(2, "Редька", "Вкусная редька по низкой цене", "20 KGZ", R.drawable.redka))
            productsList.add(Products(2, "Домашние помидоры", "Сладкие помидоры", "26 KGZ", R.drawable.tomato))
            productsList.add(Products(2, "Огурцы домашние ", "вкусные, хрустящие огурцы", "36 KGZ", R.drawable.ogurcy))
            productsList.add(Products(2, "Горький перец", "Вкусный, горький перец", "16 KGZ", R.drawable.perec_red))
            productsList.add(Products(2, "Сладкий перец", "Желтый, сладкий перец", "19 KGZ", R.drawable.perec_yellow))
            productsList.add(Products(2, "Чеснок", "Чеснок оптом", "10 KGZ", R.drawable.chesnok))
            productsList.add(Products(2, "Капуста сочная", "Свежая капуста", "29 KGZ", R.drawable.kapusta))
            productsList.add(Products(2, "Хрустящая морковка", "Сладкая морковка по низким ценам", "19 KGZ", R.drawable.morkovka))

            //Dairy products
            productsList.add(Products(3, "Сливочное масло", "вкусное, сливочное масло", "46 KGZ", R.drawable.maslo_sliv1))
            productsList.add(Products(3, "Бишкекская ряженка", "ряженка городская", "50 KGZ", R.drawable.rajenka))
            productsList.add(Products(3, "Талаское масло", "Вкусное масло из самого Таласа", "60 KGZ", R.drawable.maslo_sliv2))
            productsList.add(Products(3, "Французкий сыр", "вкусный сыр из самой Франции", "46 KGZ", R.drawable.cheeses))
            productsList.add(Products(3, "Домашнее молоко", "свежее молоко", "29 KGZ", R.drawable.moloko2))
            productsList.add(Products(3, "Нарынский творог", "вкусный Нарынский творог", "39 KGZ", R.drawable.tvorog1))
            productsList.add(Products(3, "Молоко", "Свежее молоко", "35 KGZ", R.drawable.moloko))
            productsList.add(Products(3, "Вкусные сливки", "Домашние вкусные сливки", "36KGZ", R.drawable.slivki))
            productsList.add(Products(3, "Домашний творог", "Вкусный творог", "19 KGZ", R.drawable.tvorog2))

            //Bakery
            productsList.add(Products(4, "Свежий хлеб ", "Вкусный хлеб", "20 KGZ", R.drawable.hleb1))
            productsList.add(Products(4, "Хлеб ", "Свежий хлеб", "16 KGZ", R.drawable.hleb2))
            productsList.add(Products(4, "Белый хлеб", "Домашний хлеб", "19 KGZ", R.drawable.hleb3))
            productsList.add(Products(4, "Домашний хлеб", "Белый, черный хлеб", "26 KGZ", R.drawable.hleb4))
            productsList.add(Products(4, "Свежий хлеб", "Хлеб", "26 KGZ", R.drawable.hleb5))
            productsList.add(Products(4, "Сладкий кекс", "Вкусные кексы на заказ", "36 KGZ", R.drawable.cacs))
            productsList.add(Products(4, "Свежие, булочки", "Вкусные булочки", "30 KGZ", R.drawable.bulocka1))
            productsList.add(Products(4, "Булочки домашние", "Вкусные домашние булочки", "36 KGZ", R.drawable.bulocka2))
            productsList.add(Products(4, "Сладкие булочки", "Домашние сладкие булочки", "19 KGZ", R.drawable.bulochka3))

            //Flowers
            productsList.add(Products(5, "Домашние цветы", "Красивые, домашние цветы", "100 KGZ", R.drawable.flow1))
            productsList.add(Products(5, "Цветы", "Цветы в горшочках", "160 KGZ", R.drawable.flow2))
            productsList.add(Products(5, "Все виды цветов", "Домашние цветы и букеты", "200 KGZ", R.drawable.flow3))
            productsList.add(Products(5, "Сукуленты, ромашки..", "Цветы всех видов", "200 KGZ", R.drawable.flow4))
            productsList.add(Products(5, "Цветы", "Домашние цветы и букеты", "190 KGZ", R.drawable.flow5))
            productsList.add(Products(5, "Розы, пионы..", "Цветы всех видом", "260 KGZ", R.drawable.flow6))
            productsList.add(Products(5, "Домашние цветы", "Цветы всех видом", "160 KGZ", R.drawable.flow7))
            productsList.add(Products(5, "Экзотические растения", "Растения в горшочках", "360 KGZ", R.drawable.flow8))
            productsList.add(Products(5, "Цветы, букеты и др", "Красивые цветы", "200 KGZ", R.drawable.rozy1))
        }
}