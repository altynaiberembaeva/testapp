package com.altynaiberembaeva.testapp.data

class ProductCategory {

     var id: Int
     var name: String

    constructor(productId: Int, productName: String) {
        id = productId
        name = productName
    }

    fun getProductName(): String {
        return name
    }
}
