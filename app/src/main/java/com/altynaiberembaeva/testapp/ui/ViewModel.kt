package com.altynaiberembaeva.testapp.ui

import androidx.lifecycle.MutableLiveData
import com.altynaiberembaeva.testapp.model.Model
import com.altynaiberembaeva.testapp.data.ProductCategory
import com.altynaiberembaeva.testapp.data.Products
import androidx.lifecycle.ViewModel

class ViewModel(private val model: Model = Model()) : ViewModel() {

     val liveDataCategory: MutableLiveData<List<ProductCategory>> = MutableLiveData()
     val liveDataProducts: MutableLiveData<List<Products>> = MutableLiveData()

    init {

        liveDataCategory.value = model.productCategoryList
        liveDataProducts.value = model.productsList
    }

    fun selectCategory(categoryId: Int){
        val selectedProducts = ArrayList<Products>()
        model.productsList.forEach({
            if(it.categoryId == categoryId) {
                selectedProducts.add(it)
            }
        })
        liveDataProducts.value = selectedProducts
    }
}