package com.altynaiberembaeva.testapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.altynaiberembaeva.testapp.R
import com.altynaiberembaeva.testapp.adapters.CategoryAdapterClickListener
import com.altynaiberembaeva.testapp.adapters.CategoryAdapters
import com.altynaiberembaeva.testapp.adapters.ProductsAdapters
import com.altynaiberembaeva.testapp.data.ProductCategory
import com.altynaiberembaeva.testapp.data.Products

class MainActivity : AppCompatActivity() {

    private val viewModel: ViewModel by viewModels()
    lateinit var productCategoryAdapter: CategoryAdapters
    lateinit var catRecycler: RecyclerView
    lateinit var prodItemRecycler: RecyclerView
    lateinit var productAdapter: ProductsAdapters
    lateinit var categoruClikcListener: CategoryAdapterClickListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        subscribeLiveData()
    }

    private fun subscribeLiveData(){

        viewModel.liveDataCategory.observe(this,{
            setProductRecycler(it)
        })

        viewModel.liveDataProducts.observe(this,{
            setProdItemRecycler(it)
        })
    }

    private fun setProductRecycler(productCategoryList: List<ProductCategory>) {
        catRecycler = findViewById(R.id.rv_category)
        val layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        catRecycler.layoutManager = layoutManager
        categoruClikcListener = object: CategoryAdapterClickListener{
            override fun onCategoryClick(id: Int) {
                viewModel.selectCategory(id)
            }
        }
        productCategoryAdapter = CategoryAdapters(this, productCategoryList, categoruClikcListener)
        catRecycler.adapter = productCategoryAdapter
        productCategoryAdapter.notifyDataSetChanged()
    }

    private fun setProdItemRecycler(productsList:List<Products>) {
        prodItemRecycler = findViewById(R.id.rv_products)
        productAdapter = ProductsAdapters(this, productsList)
        prodItemRecycler.adapter = productAdapter
        productAdapter.notifyDataSetChanged()
    }

    fun PrevClick(v: View) {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}

