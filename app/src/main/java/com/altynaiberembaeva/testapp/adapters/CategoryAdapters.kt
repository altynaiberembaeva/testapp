package com.altynaiberembaeva.testapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.altynaiberembaeva.testapp.R
import com.altynaiberembaeva.testapp.data.ProductCategory

class CategoryAdapters : RecyclerView.Adapter<CategoryAdapters.ProductViewHolder> {

    private val conxt: Context
    private val categoryList: List<ProductCategory>
    private val listener: CategoryAdapterClickListener

    constructor(
        context: Context,
        categoryList: List<ProductCategory>,
        listener: CategoryAdapterClickListener
    ) {
        conxt = context
        this.listener = listener
        this.categoryList = categoryList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view: View = LayoutInflater.from(conxt).inflate(R.layout.category, parent, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.catagoryName.text = categoryList.get(position).getProductName()
        holder.catagoryName.setOnClickListener {
            listener.onCategoryClick(categoryList.get(position).id)
        }
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    class ProductViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var catagoryName: TextView = itemView.findViewById(R.id.tv_cat_name)
    }
}

interface CategoryAdapterClickListener {

    fun onCategoryClick(id: Int)
}

