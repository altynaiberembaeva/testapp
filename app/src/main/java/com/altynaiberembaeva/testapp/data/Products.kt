package com.altynaiberembaeva.testapp.data

class Products {

    var categoryId: Int
    var name: String
    var description: String
    var price: String
    var image: Int

    constructor(productid: Int, productName: String, productDes: String, productPrice: String, productImage: Int) {
        categoryId = productid
        name = productName
        description = productDes
        price = productPrice
        image = productImage
    }

    fun getProductName(): String {
        return name
    }

    fun getProductPrice(): String {
        return price
    }

    fun getProductImage(): Int {
        return image
    }
}

