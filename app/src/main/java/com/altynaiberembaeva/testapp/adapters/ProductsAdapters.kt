package com.altynaiberembaeva.testapp.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.altynaiberembaeva.testapp.ProductDetails
import com.altynaiberembaeva.testapp.R
import com.altynaiberembaeva.testapp.data.Products

class ProductsAdapters : RecyclerView.Adapter<ProductsAdapters.ProductViewHolder> {

    private val conxt: Context
    private val productsList: List<Products>

    constructor(context: Context, proList: List<Products>) {
        conxt = context
        this.productsList = proList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view: View = LayoutInflater.from(conxt).inflate(R.layout.product_card, parent, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.prodImage.setImageResource(productsList.get(position).getProductImage())
        holder.prodName.text = productsList.get(position).getProductName()
        holder.prodPrice.text = productsList.get(position).getProductPrice()

        holder.itemView.setOnClickListener {
            val i = Intent(conxt, ProductDetails::class.java)
            conxt.startActivity(i)
        }
    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    class ProductViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var prodImage: ImageView
        internal var prodName: TextView
        internal var prodPrice: TextView

        init {
            prodImage = itemView.findViewById(R.id.iv_product)
            prodName = itemView.findViewById(R.id.tv_description)
            prodPrice = itemView.findViewById(R.id.tv_price)
        }
    }
}
